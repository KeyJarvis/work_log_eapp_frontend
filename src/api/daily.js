import { put, get, post } from '@/utils/request'

// 新建日报
const createDaily = (data) => post('/daily/create', data)

// 根据日期获取日报
const getDaily = (data) => get('/daily/get', data)

// 更新日报
const updateDaily = (data) => put('/daily/update', data)

// 获取未交名单
const getLack = (data) => get('/daily/lack', data)

// 获取日报统计数据
const getStatistic = (data) => get('/daily/statistic', data)

// 获取用户管理的分组
const getGroups = (data) => get('/daily/groups', data)

// 导出日报
const exportLog = (data) => post('/daily/export', data)

// 获取导出范围
const getRange = (data) => get('/daily/range', data)

export {
  getDaily,
  createDaily,
  updateDaily,
  getLack,
  getStatistic,
  getGroups,
  exportLog,
  getRange
}
