import { get } from '@/utils/request'

// 钉钉免登
const login = (data) => get('/login', data)

// 更新用户和部门数据
const update = (data) => get('/tool/update', data)

export { login, update }
