import { get, post } from '@/utils/request'

// 创建管理员
const createAdmin = (data) => post('/system/createAdmin', data)

// 获取管理员列表
const getAdminList = (data) => get('/system/adminList', data)

// 删除管理员
const deleteAdmin = (data) => get('/system/deleteAdmin', data)

// 添加白名单
const createWhite = (data) => post('/system/createWhite', data)

// 获取白名单列表
const getWhiteList = (data) => get('/system/whiteList', data)

// 删除白名单
const deleteWhite = (data) => get('/system/deleteWhite', data)

export {
  createAdmin,
  getAdminList,
  createWhite,
  getWhiteList,
  deleteAdmin,
  deleteWhite

}
