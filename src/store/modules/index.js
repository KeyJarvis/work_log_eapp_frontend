import daily from './daily'
import tool from './tool'
import system from './system'

export default {
  daily,
  tool,
  system
}
