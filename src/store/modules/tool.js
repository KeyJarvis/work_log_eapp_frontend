import { update, login } from '@/api/tool.js'

const state = {
  userInfo: {}
}
const actions = {

  async fetchUpdate({ commit }, payload) {
    let res = await update()
    return res
  },

  async fetchLogin({ commit }, payload) {
    let code = payload
    let res = await login({ code })
    if (res.status === 200) {
      commit({
        type: 'setUserInfo',
        data: res.data.userInfo
      })
      localStorage.setItem('token', res.data.token)
      localStorage.setItem('userid', res.data.userid)
    }
    return res
  }

}
const mutations = {
  setUserInfo(state, payload) {
    state.userInfo = payload.data
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
