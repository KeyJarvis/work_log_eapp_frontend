import { getDaily, getLack, getStatistic, exportLog, createDaily, updateDaily, getRange } from '@/api/daily.js'

const state = {}
const actions = {
  async fetchCreateDaily({ commit }, payload) {
    const data = payload
    let res = await createDaily(data)
    if (res.code === '200') {
      commit({
        type: 'setCreateDaily',
        data: res.data
      })
    }
    return res
  },

  async fetchGetDaily({ commit }, payload) {
    const data = payload
    let res = await getDaily({ date: data })
    return res
  },

  async fetchUpdateDaily({ commit }, payload) {
    const data = payload
    let res = await updateDaily(data)
    return res
  },

  async fetchGetLack({ commit }, payload) {
    let res = await getLack()
    return res
  },

  async fetchGetStatistic({ commit }, payload) {
    let res = await getStatistic()
    return res
  },

  async fetchGetRange({ commit }, payload) {
    let res = await getRange()
    return res
  },

  async fetchExportLog({ commit }, payload) {
    const data = payload
    let res = await exportLog(data)
    return res
  }
}

const mutations = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
