import { createAdmin, createWhite, getAdminList, getWhiteList, deleteAdmin, deleteWhite } from '@/api/system.js'

const state = {}

const actions = {
  async fetchCreateAdmin({ commit }, payload) {
    const data = payload
    let res = await createAdmin(data)
    return res
  },
  async fetchCreateWhite({ commit }, payload) {
    const data = payload
    let res = await createWhite(data)
    return res
  },
  async fetchGetAdminList({ commit }, payload) {
    let res = await getAdminList()
    return res
  },
  async fetchGetWhiteList({ commit }, payload) {
    let res = await getWhiteList()
    return res
  },
  async fetchDeleteAdmin({ commit }, payload) {
    let name = payload
    let res = await deleteAdmin({ name })
    return res
  },
  async fetchDeleteWhite({ commit }, payload) {
    let userid = payload
    let res = await deleteWhite({ userid })
    return res
  }
}

const mutations = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
