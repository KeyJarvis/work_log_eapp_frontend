import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    // 首页
    {
      path: '/',
      name: '填写日报',
      component: () => import('@/pages/dailyLog/addWorkLog.vue')
    },
    // 补交日报
    {
      path: '/daily/repair-workLog',
      name: '补交日报',
      component: () => import('@/pages/dailyLog/repairWorkLog.vue')
    },
    // 查看日报
    {
      path: '/daily/view-workLog',
      name: '查看日报',
      component: () => import('@/pages/dailyLog/viewWorkLog.vue')
    },
    // 系统管理员设置
    {
      path: '/system/admin-setting',
      name: '系统管理员设置',
      component: () => import('@/pages/systemSetting/adminSetting.vue')
    },
    // 日报提交人员设置
    {
      path: '/system/white-setting',
      name: '白名单设置',
      component: () => import('@/pages/systemSetting/whiteSetting.vue')
    }
  ]
})
