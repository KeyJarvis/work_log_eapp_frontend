# 基础镜像
FROM node:lts-alpine as build-stage

# 设置工作目录
WORKDIR /app/frontend

# 拷贝package.json到工作目录
COPY package*.json ./

# 设置淘宝镜像源
RUN npm install -g cnpm --registry=https://registry.npm.taobao.org

# 安装项目依赖
RUN cnpm install

COPY . .

# 构建项目
RUN npm run build
