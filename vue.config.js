const path = require('path')

module.exports = {
  publicPath: '/',
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.join(__dirname, 'src') // 别名设置
      }
    }
  },
  devServer: {
    port: 8080,
    disableHostCheck: true,
    proxy: {
      '^/prefix_need_to_proxy': {
        target: '<url>',
        ws: true,
        changeOrigin: true
      }
    }
  },
  chainWebpack: config => {
    config.module
      .rule('pug')
      .test(/\.pug$/)
      .use('pug-plain-loader')
      .loader('pug-plain-loader')
      .end()
  }
}
